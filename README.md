# Kubectl

`kubectl` docker image useful for K8S deploy

## Use `kubectl` image for K8S Deploy in `gitlab-ci.yml`

```
deploy_production:
  stage: deploy
  environment:
    name: production
    kubernetes:
      namespace: my-namespace
  image: registry.gitlab.com/nexylan/docker/kubectl:X.Y.Z # <--- Change version here !
  script:
    - kubectl apply -f deployment.yaml
```
