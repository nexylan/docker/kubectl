FROM registry.gitlab.com/nexylan/docker/core:3.1.0-alpine

RUN apk add --no-cache \
    gettext=~0.20.2 \
    curl=~7.74
RUN curl --location https://dl.k8s.io/release/v1.24.2/bin/linux/amd64/kubectl -o /usr/local/bin/kubectl
RUN chmod +x /usr/local/bin/kubectl
